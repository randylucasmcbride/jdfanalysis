var mongojs = require('mongojs');
var db = mongojs('admin:Pr1nt1ng@ds031972.mongolab.com:31972/jdfanalysis', ['import', 'arch']);
var async = require('async');

console.log('Reading records from import collection')
db.import.find(function(err, docs){	
	if(err){
		console.log("Couldn't query import collection in DB. Error: " + err)
	}else{
		console.log('Saving files to arch collection');
		var copyCount = 0;
		async.each(docs, function(doc, callback){
			(function(d){
				db.arch.save(doc, function(err){
					if(err){
						callback("Fail. Couldn't save " + d.id + " to 'arch' collection.")
					}else{
						copyCount += 1;
						callback();
					}
				});
			}(doc));
		}, function(err){
			if(err){
				console.log(err)
			}else{
				console.log('Success. ' + copyCount + ' records copied from import to arch.')
				db.close();
			}
		})
	}
})