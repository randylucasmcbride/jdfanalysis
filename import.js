var fs = require('graceful-fs');
var parseString = require('xml2js').parseString;
var mongojs = require('mongojs');
var db = mongojs('admin:Pr1nt1ng@ds031972.mongolab.com:31972/jdfanalysis', ['import']);
var async = require('async');


//TODO pull path as arg
if(process.argv[2]){
	var _path = process.argv[2];
	console.log("Clearing 'import' collection on DB")
	db.import.remove(function(err){
		if(err){
			console.log("Fail. Couldn't clear 'import' collection in DB. Error: " + err);
		}else{
			async.waterfall([
				function(callback){
					console.log('Reading director ' + _path)
					fs.readdir(_path, function(err, files){
						if(err){
							callback("Couldn't read directory. Error: " + err);
						}else{
							callback(null, files)
						}
					});//fs.readdir
				},//function
				function(files, callback){
					var fileContents = [];
					var fileName;
					console.log('Reading ' + files.length + ' files')
					async.each(files, function(file, callback){
						(function(f){
							fileName = _path + '\\' + f;
							fs.readFile(fileName, function(err, contents){
								if(err){
									callback("Couldn't read file. Error: " + err);
								}else{
									fileContents.push({fileName: f, body: contents})
									callback();
								}
							});
						}(file));
					}, function(err){
						if(err){
							callback(err)
						}else{
							callback(null, fileContents)
						}
					});//async.each
				},//fucntion(files, callback)
				function(fileContents, callback){
					console.log("Converting files to JSON");
					var jsonObjects = [];
					async.each(fileContents, function(content, callback){
						(function(file, body){
							parseString(body ,{attrkey: '-', explicitArray: false, mergeAttrs: true},function(err, js){
								if(err){
									fs.writeFile('json-log.txt', content.body, function(err){
										if(err){
											callback("Couldn't log parse-error in 'json-log.txt'. Error: " + err)
										}else{
											callback("Couldn't convert file " + file + " to JSON. JSON content logged to 'json-log.txt'. Error: " + err)
										}
									});
								}else{
									jsonObjects.push({fileName: content.fileName, body: js})
									callback();
								}
							})//parseString
						}(content.fileName, content.body));

					}, function(err){
						if(err){
							callback(err)
						}else{
							callback(null, jsonObjects)
						}
					});//async.each
				},//function(fileContents)
				function(jsonObjects, callback){
					console.log('Saving JSON to database');
					var result = {
						uploadCount: 0,
						emptyFiles: []
					};
					async.each(jsonObjects, function(js, callback){
						(function(file, body){
							if(body){
								db.import.save(body, function(err){
									if(err){
										fs.writeFile('json-log.txt', js.body, function(err){
											if(err){
												callback("Couldn't log save error in 'json-log.txt'. Error: " + err)
											}else{
												callback("Couldn't save file" + file + " to Database. JSON Content logged to 'json-log.txt'. Error: " + err)
											}
										});
									}else{
										result.uploadCount += 1;
										callback();
									}
								});//db.import.save
							}else{//if(js.body)
								result.emptyFiles.push(file);
								callback();
							}
						}(js.fileName, js.body));
					}, function(err){
						if(err){
							callback(err)
						}else{
							callback(null, result);
						}
					})//async.each
				}
			], function(err, result){
				if(err){
					console.log('Fail. Script could not complete.')
					console.log(err)
				}else{
					console.log('Success. Uploaded ' + result.uploadCount + ' files to database');
					if(result.emptyFiles.length > 0){
						console.log('The following file(s) w(as)(ere) not saved to the database:');
						result.emptyFiles.forEach(function(file){
							console.log(file);
						})
					}
					console.log('Closing connection to database . . .')
					db.close();
					console.log('. . .Closed. Goodbye.')
				}
			});

		}
	});
}else{
	console.log('Please provide path. E.g. "node import C:\\Users\\JohnDoe"');
}